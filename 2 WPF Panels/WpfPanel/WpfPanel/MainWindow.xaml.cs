﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfPanel
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
       
        private void BtnCanvas_Click(object sender, RoutedEventArgs e)
        {
            CanvasWindow caw = new CanvasWindow();
            caw.Show();
        }

        private void BtnWrapPanel_Click(object sender, RoutedEventArgs e)
        {

            WrapPanelWindow wpw = new WrapPanelWindow();
            wpw.Show();

        }

        private void BtnStackPanel_Click(object sender, RoutedEventArgs e)
        {
            StackPanelWindow spw = new StackPanelWindow();
            spw.Show();
        }

        private void BtnDockPanel_Click(object sender, RoutedEventArgs e)
        {
            DockPanelWindow dpw = new DockPanelWindow();
            dpw.Show();
        }

        private void BtnGrid_Click(object sender, RoutedEventArgs e)
        {
            GridWindow gw = new GridWindow();
            gw.Show();
        }

      
    }
}

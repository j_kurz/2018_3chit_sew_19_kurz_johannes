﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace SÜ_26_02_2019_WpfÜbungen1
{
    /// <summary>
    /// Interaktionslogik für WindowTextBlock.xaml
    /// </summary>
    public partial class WindowTextBlock : Window
    {
        public WindowTextBlock()
        {
            InitializeComponent();
            ReadFile(@"../Kopiertertext.txt");
            this.Show();
        }
        public void ReadFile(string filname)
        {
            string s = File.ReadAllText(filname);
            txtcopytext.Text = s;
        }

      
    }
}

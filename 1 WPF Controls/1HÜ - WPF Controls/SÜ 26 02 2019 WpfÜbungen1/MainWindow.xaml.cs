﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SÜ_26_02_2019_WpfÜbungen1
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            WindowTextBlock wtb = new WindowTextBlock();
            wtb.Show();
            
            MessageBox.Show("Dies ist eine message box");
        }

        private void btnTB_Click(object sender, RoutedEventArgs e)
        {
            TextBlock tb = new TextBlock();
            tb.Show();
        }

        private void BtnLBL_Click(object sender, RoutedEventArgs e)
        {
            LBLWindow lb = new LBLWindow();
            lb.Show();
        }

        private void BtnTXTB_Click(object sender, RoutedEventArgs e)
        {
            Textbox tbx = new Textbox();
            tbx.Show();
        }

        private void BtnCB_Click(object sender, RoutedEventArgs e)
        {
            Checkbox Cb = new Checkbox();
            Cb.Show();
        }

        private void BtnRBTN_Click(object sender, RoutedEventArgs e)
        {
            Radiopannel Rp = new Radiopannel();
            Rp.Show();
        }
    }
}

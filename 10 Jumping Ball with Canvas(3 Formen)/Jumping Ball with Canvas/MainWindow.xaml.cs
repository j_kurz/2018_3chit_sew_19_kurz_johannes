﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Jumping_Ball_with_Canvas
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        public MainWindow()
        {
            InitializeComponent();
            timer.Tick += Physics;
            timer.Interval = TimeSpan.FromSeconds(0.05);
        }

        private void ButtonStartStop_Click(object sender, RoutedEventArgs e)
        {
            timer.IsEnabled = !timer.IsEnabled;
        }
        bool GoingRight = true;
        bool GoingDown = true;
        private void Physics(object sender, EventArgs e)
        {
            double speed = 3.0;
            if(CheckBoxFast.IsChecked.Value)
            {
                speed = 10.0;
            }
            MoveBallLeftRight(speed);
            MoveBallUpDown(speed);
            MoveRectLeftRight(speed );
            MoveRectUpDown(speed) ;
            MoveTriLeftRight(speed );
            MoveTriUpDown(speed );
        }
        private void MoveBallLeftRight(double speed)
        {
            double x = Canvas.GetLeft(Ball);
            if(GoingRight)
            {
                x += speed;
            }
            else
            {
                x -= speed;
            }

            if(x+Ball.Width > TheCanvas.ActualWidth)
            {
                GoingRight = false;
                x = TheCanvas.ActualWidth - Ball.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x<0.0)
            {
                GoingRight = true;
                x = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }
            Canvas.SetLeft(Ball, x);
        }
        private void MoveBallUpDown(double speed)
        {
            double y = Canvas.GetTop(Ball);
            if (GoingDown)
            {
                y += speed;
            }
            else
            {
                y -= speed;
            }

            if (y + Ball.Height > TheCanvas.ActualHeight)
            {
                GoingDown = false;
                y = TheCanvas.ActualHeight - Ball.Height;
                System.Media.SystemSounds.Exclamation.Play();
            }
            else if (y < 0.0)
            {
                GoingDown = true;
                y = 0.0;
                System.Media.SystemSounds.Hand.Play();
            }
            Canvas.SetTop(Ball, y);
        }
        int score = 0;
        private void Ball_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (timer.IsEnabled)
            {
                score++;
                labelScoue.Content = score;
            }
        }
        private void MoveRectLeftRight(double speed)
        {
            double x = Canvas.GetLeft(Rect);
            if (GoingRight)
                x += speed;
            else
                x -= speed;
            if (x + Rect.Width > TheCanvas.ActualWidth)
            {
                GoingRight = false;
                x = TheCanvas.ActualWidth - Rect.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0.0)
            {
                GoingRight = true;
                x = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }
            Canvas.SetLeft(Rect, x);
        }
        private void MoveRectUpDown(double speed)
        {
            double x = Canvas.GetTop(Rect);
            if (GoingDown)
                x += speed;
            else
                x -= speed;
            if (x + Rect.Height > TheCanvas.ActualHeight)
            {
                GoingDown = false;
                x = TheCanvas.ActualHeight - Rect.Height;
                System.Media.SystemSounds.Exclamation.Play();
            }
            else if (x < 0.0)
            {
                GoingDown = true;
                x = 0.0;
                System.Media.SystemSounds.Hand.Play();
            }
            Canvas.SetTop(Rect, x);
        }
        private void Rect_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (timer.IsEnabled)
            {
                score++;
                labelScoue.Content = score;
            }
        }
        private void MoveTriLeftRight(double speed)
        {
            double x = Canvas.GetLeft(Tri);
            if (GoingRight)
                x += speed;
            else
                x -= speed;
            if (x + Tri.Width > TheCanvas.ActualWidth)
            {
                GoingRight = false;
                x = TheCanvas.ActualWidth - Tri.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0.0)
            {
                GoingRight = true;
                x = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }
            Canvas.SetLeft(Tri, x);
        }
        private void MoveTriUpDown(double speed)
        {
            double x = Canvas.GetTop(Tri);
            if (GoingDown)
                x += speed;
            else
                x -= speed;
            if (x + Tri.Height > TheCanvas.ActualHeight)
            {
                GoingDown = false;
                x = TheCanvas.ActualHeight - Tri.Height;
                System.Media.SystemSounds.Exclamation.Play();
            }
            else if (x < 0.0)
            {
                GoingDown = true;
                x = 0.0;
                System.Media.SystemSounds.Hand.Play();
            }
            Canvas.SetTop(Tri, x);
        }

        private void Tri_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (timer.IsEnabled)
            {
                score++;
                labelScoue.Content = score;
            }
        }




        private void radioButtonBlue_Click(object sender, RoutedEventArgs e)
        {
            
                Ball.Fill = Brushes.Blue;
                Rect.Fill = Brushes.Blue;
                Tri.Fill = Brushes.Blue;
            
        }

        private void radioButtonRed_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Red;
            Rect.Fill = Brushes.Red;
            Tri.Fill = Brushes.Red;
        }

        private void radioButtonGreen_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Green;
            Rect.Fill = Brushes.Green;
            Tri.Fill = Brushes.Green;
        }

        private void RadioButtonBlue_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void RadioButtonRed_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void RadioButtonGreen_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FazVerlag
{
    abstract class PublishingCompany
    {
        private List<ISubscriber> subscriberlist = new List<ISubscriber>();

        public void Subscribe(ISubscriber s)
        {
            subscriberlist.Add(s);
        }
        public void Remove(ISubscriber s)
        {
            subscriberlist.Remove(s);
        }
        public void SendNews(NewsPaper newspaper)
        {
         foreach(ISubscriber s in subscriberlist)
            {
                s.Recieve(newspaper);
            }
        }
    }
}

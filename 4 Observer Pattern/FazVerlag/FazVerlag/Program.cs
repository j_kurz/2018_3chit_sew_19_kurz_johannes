﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FazVerlag
{
    class Program
    {
        static void Main(string[] args)
        {
            FAZVerlag fv = new FAZVerlag();
            FamilyKurz fam1 = new FamilyKurz();
            fv.Subscribe(fam1);
            fv.Subscribe(fam1);
            fv.SendNews(new NewsPaper("Wieso ist der Himmel blau ?"));
            fv.Remove(fam1);
            fv.SendNews(new NewsPaper("Wieso lesen immer weniger Leute Zeitung ?"));
        }
    }
}

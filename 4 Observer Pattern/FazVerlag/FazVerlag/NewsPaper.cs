﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FazVerlag
{
    class NewsPaper
    {
        private string titel;
        public NewsPaper(string titel)
        {
            this.titel = titel;
        }
        public string getTitel()
        {
            return titel;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_Pattern
{
    public abstract class MenuComponent
    {
        public string Name { get; set; }
        public string Description { get;  set;}
        public int Id { get; set; }
        public MenuComponent(string name,string description,int id)
        {
            Name = name;
            Description = description;
            Id = id;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("");
            sb.AppendLine($"ID : {Id} ");
            sb.AppendLine($"Name : {Name} ");
            sb.AppendLine($"Description : {Description} ");
            return sb.ToString();
        }
        public abstract IEnumerator<MenuComponent> CreateIterator();
        
    }
}

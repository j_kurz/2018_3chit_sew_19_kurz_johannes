﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_Pattern
{
    public class Menu:MenuComponent
    {
        public List<MenuComponent> Menulist { get; set; }
        public Menu(string name, string description, int id) : base(name, description, id) {
            Menulist = new List<MenuComponent>();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Menü :");
            sb.Append($"{base.ToString()}");
            //foreach(MenuComponent mc in Menulist)
            //{
            //    sb.AppendLine($"{mc} \n");
            //}
            IEnumerator<MenuComponent> iterator = Menulist.GetEnumerator();
            while(iterator.MoveNext())
            {
                sb.Append(iterator.Current.ToString());
;            }
            
                return sb.ToString();
        }
        

        public override IEnumerator<MenuComponent> CreateIterator()
        {
            return Menulist.GetEnumerator();
        }
    }

}
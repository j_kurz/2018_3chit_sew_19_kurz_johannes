﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu m1 = new Menu("Standard Menu","very good ",0);
            Menu AMenu = new Menu("Diner Menu", "Very heatly", 1);
            Menu BMenu = new Menu("Cafe Menu", "Popular in Austria", 2);
            Menu CMenu = new Menu("Dessert Menu", "Our Desserts", 3);
            AMenu.Menulist.Add(new MenuItem(false,7.5,"Schnitzel",1,"Very good"));
            BMenu.Menulist.Add(new MenuItem(true, 7.5, "Kaffee Schwarz", 1, "served hot"));
            CMenu.Menulist.Add(new MenuItem(false, 3.5, "Muffin", 1, "Sweet "));
            m1.Menulist.Add(AMenu);
            m1.Menulist.Add(BMenu);
            m1.Menulist.Add(CMenu);
            Console.WriteLine(m1.ToString());
        }
    }
}

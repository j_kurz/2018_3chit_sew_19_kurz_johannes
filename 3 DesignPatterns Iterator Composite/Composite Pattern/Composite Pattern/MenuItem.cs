﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_Pattern
{
    class MenuItem:MenuComponent
    {
        public bool Vegeterian { get; set; }
        public double Price { get; set; }
        public MenuItem(bool vegeterian,double price,string name,int id,string description)  :base(name, description,id)
        {
            Vegeterian = vegeterian;
            Price = price;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base.ToString());
            sb.AppendLine($"Vegeterian ? : {Vegeterian.ToString()}");
            sb.AppendLine($"Price : {Price} Euro ");
         
            return sb.ToString();
        }
        public override IEnumerator<MenuComponent> CreateIterator()
        {
            return new Iterator();
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_WpfStudents
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public EClazz Clazz { get; set; }
        public Student(string fn, string ln, int age, EClazz clazz)
        {
            FirstName = fn;
            LastName = ln;
            Age = age;
            Clazz = clazz;
        }
      
        public string ToCSV()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(FirstName).Append(";").Append(LastName).Append(";").Append(Age).Append(";").Append(Clazz).Append(";");
            return sb.ToString();
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}

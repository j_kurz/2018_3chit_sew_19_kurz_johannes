﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _8_WpfStudents
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        StudentVM stvm = new StudentVM();
        public MainWindow()
        {
            InitializeComponent();
            stvm.Initialize();
            dataGridStudents.ItemsSource = stvm.Students;
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {



            stvm.Students.Add(new Student(txtFN.Text, txtLN.Text, (Convert.ToInt32(txtAge.Text)), (EClazz)Enum.Parse(typeof(EClazz), cmbClass.Text)));

            txtFN.Text = "";
            txtLN.Text = "";
            txtAge.Text = "";
            cmbClass.Text = "";
            stvm.Save();
        }
    }
}

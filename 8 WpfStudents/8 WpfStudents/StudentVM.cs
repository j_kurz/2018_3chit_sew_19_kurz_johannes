﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.IO;

namespace _8_WpfStudents
{
    public enum EClazz { _1CHIT, _2BHIT, _3CHIT, _4CHIT, _5CHIT }
    public class StudentVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Student> Students { get; set; }
      
        string path = "students.csv";
        
       
        public StudentVM()
        {
            Students = new ObservableCollection<Student>();
        }
        public void Initialize()
        {
            ReadCSV();
        }
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
       
        public void Save()
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                foreach (Student item in Students)
                    sw.WriteLine(item.ToCSV());
            }
        }
        public void ReadCSV()
        {
            StreamReader sr = new StreamReader(path);
            string[] line;
            while (!sr.EndOfStream)
            {
                line = (sr.ReadLine()).Split(';');
                Students.Add(new Student(line[0], line[1], Convert.ToInt32(line[2]), (EClazz)Enum.Parse(typeof(EClazz), line[3])));
            }
            sr.Close();
        }
        

    }


}

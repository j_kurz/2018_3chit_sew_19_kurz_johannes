﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Drawing;

namespace ColorExample
{
    public class SliderVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private int rvalue;
        private int gvalue;
        private int bvalue;
        public ICommand RandomColour { get; set; }

        public SliderVM()
        {
            rvalue = 127;
            gvalue = 127;
            bvalue = 127;
            RandomColour= new RandomColour(this);
        }

        public int RValue
        {
            get { return rvalue; }
            set
            {
                if (value > 255)
                    rvalue = 255;
                else if (value < 0)
                    rvalue = 0;
                else
                    rvalue = value;
                OnPropertychanged();
                OnPropertychanged("BackgroundColor");
            }
        }

        public int GValue
        {
            get { return gvalue; }
            set
            {
                if (value > 255)
                    gvalue = 255;
                else if (value < 0)
                    gvalue = 0;
                else
                    gvalue = value;
                OnPropertychanged();
                OnPropertychanged("BackgroundColor");
            }
        }

        public int BValue
        {
            get { return bvalue; }
            set
            {
                if (value > 255)
                    bvalue = 255;
                else if (value < 0)
                    bvalue = 0;
                else
                    bvalue = value;
                OnPropertychanged();
                OnPropertychanged("BackgroundColor");
            }
        }

        public string BackgroundColor
        {
            get
            {
                return ColorTranslator.ToHtml(Color.FromArgb(rvalue, gvalue, bvalue));
            }
        }

        public void OnPropertychanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}


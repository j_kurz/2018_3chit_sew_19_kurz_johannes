﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ColorExample
{
    public class RandomColour : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private SliderVM slider;
        private Random r;
        public RandomColour(SliderVM slider)
        {
            this.slider = slider;
            r = new Random();
            slider.PropertyChanged += delegate { CanExecuteChanged(this, EventArgs.Empty); };
        }
        

        public bool CanExecute(object para)
        {
            return (slider != null);
        }

        public void Execute(object parameter)
        {
            slider.RValue = r.Next(0, 256);
            slider.GValue = r.Next(0, 256);
            slider.BValue = r.Next(0, 256);
        }
    }
}

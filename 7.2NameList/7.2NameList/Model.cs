﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _7._2NameList
{
    public class Model : INotifyPropertyChanged
    {
        private string currentName;
        public string CurrentName
        {
            get { return currentName; }
            set
            {
                if (value == currentName)
                    return;
                currentName = value;
                OnPropertyChanged();
            }
        }

        public Model()
        {
            AddCommand = new AddNameCommand(this);
        }
        public ICommand AddCommand { get; private set; }

        public ObservableCollection<string> AddedNames { get; } = new ObservableCollection<string>();
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

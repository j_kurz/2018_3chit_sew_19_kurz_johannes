﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _7._2NameList
{
    class AddNameCommand:ICommand
    {
        Model parent;
        public AddNameCommand(Model parent)
        {
            this.parent = parent;
            parent.PropertyChanged += delegate { CanExecuteChanged?.Invoke(this, EventArgs.Empty);};
        }
        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter) { return !string.IsNullOrEmpty(parent.CurrentName); }
        public void Execute(object parameter)
        {
            parent.AddedNames.Add(parent.CurrentName);
            parent.CurrentName= null;
        }

    }
}

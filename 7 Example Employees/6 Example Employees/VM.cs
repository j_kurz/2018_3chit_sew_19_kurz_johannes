﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace _6_Example_Employees
{
    class VM:INotifyPropertyChanged
    {
        private ObservableCollection<Employee> employees = new ObservableCollection<Employee>();

        public event PropertyChangedEventHandler PropertyChanged;
        public VM()
        {
            
            
        //ObservableCollection Daten erzeugen

        employees.Add(new Employee { MemberID = 1, Name = "John Hancock", Department = "IT", Phone = "31234743", Email = @"John.Hancock@Company.com", Salary = "3450.44" });
            employees.Add(new Employee { MemberID = 2, Name = "Jane Hayes", Department = "Sales", Phone = "31234744", Email = @"Jane.Hayes@Company.com", Salary = "3700" });
            employees.Add(new Employee { MemberID = 3, Name = "Larry Jones", Department = "Marketing", Phone = "31234745", Email = @"Larry.Jones@Company.com", Salary = "3000" });
            employees.Add(new Employee { MemberID = 4, Name = "Patricia Palce", Department = "Secretary", Phone = "31234746", Email = @"Patricia.Palce@Company.com", Salary = "2900" });
            employees.Add(new Employee { MemberID = 5, Name = "Jean L. Trickard", Department = "Director", Phone = "31234747", Email = @"Jean.L.Tricard@Company.com", Salary = "5400" });
        }
        private void OnPropertyChanged(string Property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Property));
        }
        public ObservableCollection<Employee> Employees
        {
            get
            {
                return employees;
            }
            set
            {
                employees = value;
                OnPropertyChanged("Employees");
            }
        }
    }
}

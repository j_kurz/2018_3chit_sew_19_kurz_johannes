﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SkillList
{
    class Add:ICommand
    {
        public event EventHandler CanExecuteChanged;
        private SkillVM parent;

        public Add(SkillVM parent)
        {
            this.parent = parent;
            parent.PropertyChanged += delegate { CanExecuteChanged(this, EventArgs.Empty); };
        }

        public bool CanExecute(object parameter)
        {
            return (parent.SkillName != "" && parent.SkillName != null);
        }

        public void Execute(object parameter)
        {
            parent.Skills.Add(new Skill(parent.SkillName, 0));
            parent.SkillName = "";
        }
    }
}

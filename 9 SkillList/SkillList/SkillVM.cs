﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SkillList
{
    class SkillVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<Skill> skills;
        private string skillname;
        public ICommand ACommand { get; set; }
        public ICommand APCommand { get; set; }
        public ICommand RPCommand { get; set; }
        private Skill currentSkill;


        public SkillVM()
        {
            Skills = new ObservableCollection<Skill>();
            skillname = "";
            ACommand = new Add(this);
            currentSkill = new Skill("", 0);
            APCommand = new AddPercent(this);
            RPCommand = new RemovePercent(this);
        }

        public void OnPropertychanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string SkillName
        {
            get { return skillname; }
            set
            {
                skillname = value;
                OnPropertychanged();
            }
        }

        public string CurrentName
        {
            get
            {
                return CurrentSkill.Name;
            }
            set
            {
                CurrentSkill.Name = value;
                OnPropertychanged();
            }
        }

        public int CurrentPercent
        {
            get { return CurrentSkill.Percent; }
            set
            {
                CurrentSkill.Percent = value;
                OnPropertychanged();
            }
        }

        public Skill CurrentSkill
        {
            get { return currentSkill; }
            set
            {
                currentSkill = value;
                OnPropertychanged();
                OnPropertychanged("CurrentName");
            }
        }

        public ObservableCollection<Skill> Skills
        {
            get { return skills; }
            set
            {
                skills = value;
                OnPropertychanged();
            }
        }

        public void AddSkills(List<Skill> list)
        {
            foreach (Skill s in list)
            {
                skills.Add(s);
            }
        }
    }
}

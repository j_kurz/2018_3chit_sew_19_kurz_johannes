﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SkillList
{
    class RemovePercent:ICommand
    {
        public event EventHandler CanExecuteChanged;
        private SkillVM parent;

        public RemovePercent(SkillVM parent)
        {
            this.parent = parent;
            parent.PropertyChanged += delegate { CanExecuteChanged(this, EventArgs.Empty); };
        }

        public bool CanExecute(object parameter)
        {
            return parent.CurrentSkill != null;
        }

        public void Execute(object parameter)
        {
            parent.CurrentPercent = ((parent.CurrentPercent - 10) <= 0) ? 0 : parent.CurrentPercent - 10;
        }
    }
}

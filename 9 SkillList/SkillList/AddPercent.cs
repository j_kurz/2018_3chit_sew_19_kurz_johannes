﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SkillList
{
    class AddPercent:ICommand
    {
        public event EventHandler CanExecuteChanged;
        private SkillVM parent;

        public AddPercent(SkillVM parent)
        {
            this.parent = parent;
            parent.PropertyChanged += delegate { CanExecuteChanged(this, EventArgs.Empty); };
        }

        public bool CanExecute(object parameter)
        {
            return parent.CurrentSkill != null;
        }

        public void Execute(object parameter)
        {
            parent.CurrentPercent = ((parent.CurrentPercent + 10) > 100) ? 100 : parent.CurrentPercent + 10;
        }
    }
}

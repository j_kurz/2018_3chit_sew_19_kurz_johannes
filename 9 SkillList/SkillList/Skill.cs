﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SkillList
{
    class Skill : INotifyPropertyChanged
    {
        public string name;
        public int percent;
        public event PropertyChangedEventHandler PropertyChanged;

        public Skill(string name, int percent)
        {
            this.name = name;
            this.percent = percent;
        }

        public void OnPropertychanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertychanged();
            }
        }

        public int Percent
        {
            get { return percent; }
            set
            {
                percent = value;
                OnPropertychanged();
            }
        }
    }
}

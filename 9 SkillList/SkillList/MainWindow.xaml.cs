﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkillList
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SkillVM svm = new SkillVM();
            DataContext = svm;
             List<Skill> startskills = new List<Skill>()
            {
                new Skill("C#", 80),
                new Skill("Java",50),
                new Skill("Python",20),
                new Skill("Entity Framework", 60),
                new Skill("Angular",70),
                new Skill("Aurelia", 30),
                new Skill("Backbone", 10),
                new Skill("C/P", 98),
                new Skill("Ember", 0)
            };
            svm.AddSkills(startskills);
        }

     
    }
}
